import java.io.File;

import javafx.scene.control.Button; 
import javafx.scene.image.ImageView;
 import javafx.scene.image.Image;;

public class Bouton extends Button{
    
    private Case laCase;

    public Bouton(Case laCase){
        super();
        this.setPrefWidth(30);
        this.setPrefHeight(30);
        this.laCase = laCase;
        if (laCase.estRevelee() && laCase.estBombe())
            ajouteImage("img/bombe.png");
        else if (laCase.estRevelee() && laCase.estBombe())
            ajouteImage("img/bombe.png");
        
    }
    
    public void ajouteImage(String fichierImage){
        try{
                Image image = new Image(fichierImage);
                ImageView iv = new ImageView();
                iv.setImage(image);
                iv.setFitWidth(20);
                iv.setPreserveRatio(true);
                this.setGraphic(iv);
            }
        catch(Exception e){
            this.setText(laCase.getAffichage());
        }
    }

    public void maj(){
        this.setText(this.laCase.getAffichage());
        this.setGraphic(null);
        if(laCase.getAffichage().equals("@")){
            Image bombe = new Image(new File("./img/bombe.png").toURI().toString(), 10, 10, true, true);
            this.setStyle("-fx-content-display: graphic-only;");
            this.setGraphic(new ImageView(bombe));
        }
        if(laCase.getAffichage().equals("0")){
            this.setStyle("-fx-text-fill: purple; -fx-font-size: 12px; -fx-font-weight: bold;");
        }
        if(laCase.getAffichage().equals("1")){
            this.setStyle("-fx-text-fill: blue; -fx-font-size: 12px; -fx-font-weight: bold;");
        }
        if(laCase.getAffichage().equals("2")){
            this.setStyle("-fx-text-fill: green; -fx-font-size: 12px; -fx-font-weight: bold;");
        }
        if(laCase.getAffichage().equals("3")){
            this.setStyle("-fx-text-fill: red; -fx-font-size: 12px; -fx-font-weight: bold;");
        }
        if(laCase.getAffichage().equals("4")){
            this.setStyle("-fx-text-fill: black; -fx-font-size: 12px; -fx-font-weight: bold;");
        }
        if(laCase.getAffichage().equals("?")){
            Image bombe = new Image(new File("./img/flag.png").toURI().toString(), 10, 10, true, true);
            this.setStyle("-fx-content-display: graphic-only;");
            this.setGraphic(new ImageView(bombe));
        }

        if (this.laCase.estRevelee())
            this.setDisable(true);
        else
            this.setDisable(false);
        this.setText(this.laCase.getAffichage());
    }
}
