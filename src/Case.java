import java.util.*;

public class Case {

    private boolean bombe, revelee, marquee;
    private int coordLigne, cordColonne;
    private List<Case> casesVoisines;

    public Case(int coordLigne, int coordColonne) {
        this.bombe = false;
        this.revelee = false;
        this.marquee = false;
        this.coordLigne = coordLigne;
        this.cordColonne = coordColonne;
        casesVoisines = new ArrayList<>();
    }

    // Getters
    public boolean estBombe(){return this.bombe;}
    public boolean estRevelee(){return this.revelee;}
    public boolean estMarquee(){return this.marquee;}
    public int getCoordLigne(){return this.coordLigne;}
    public int getCoordColonne(){return this.cordColonne;}

    //Setters
    public void ajouteBombe(){this.bombe = true;}
    public void reveler(){this.revelee = true;}
    public void marquer(){this.marquee = true;}

    public List<Case> getCaseVoisines(){return casesVoisines;}

    /*
     * Ajoute les cases voisines à la liste correspondante
     */
    public void ajouteCaseVoisine(Case caseVoisine){
        casesVoisines.add(caseVoisine);
    }

    /*
     * Retourne le nombre de bombes voisines
     */
    public int getNbBombesVoisine(){
        int nbBombesVoisine = 0;
        for (Case case1 : casesVoisines){
            if (case1.estBombe()){
                ++ nbBombesVoisine;
            }
        }
        return nbBombesVoisine;
    }

    public String getAffichage(){
        if (estRevelee() && estBombe()){
            return "@";
        }
        if (estMarquee()){
            return "?";
        }
        if (estRevelee()){
            return String.valueOf(this.getNbBombesVoisine());
        }
        if (!(estRevelee() && estMarquee())){
            return " ";
        }
        return "";
    }


    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(obj instanceof Case){
            Case case1 = (Case)(obj);
            return case1.getCoordLigne() == this.getCoordLigne() && case1.getCoordColonne() == getCoordColonne();
        }
        return false;
    }

}