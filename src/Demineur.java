import java.util.Scanner;

public class Demineur {

    public static String action() throws ErreurEntreException{
        Scanner sc = new Scanner(System.in);
        System.out.println("Voulez-vous Révéler (r) ou Marquer (m) une case ?");
        String res = sc.nextLine();
        if(!(Character.toLowerCase(res.charAt(0)) == 'r') && !(Character.toLowerCase(res.charAt(0)) == 'm')){
            throw new ErreurEntreException("Veuillez entrer 'r' pour Révéler ou 'm' pour Marquer une case !");
        }
        else{
            return res;
        }
    }

    public static int jouerLigne(String action, Grille grille) throws ErreurEntreException{
        Scanner sc = new Scanner(System.in);
        if(action.equals("r")){
            System.out.println("A quelle ligne se trouve la case que souhaitez vous révéler ?");
        }
        else if(action.equals("m")){
            System.out.println("A quelle ligne se trouve la case que souhaitez vous marquer ?");
        }
        String res = sc.nextLine();
        if(!Character.isDigit(res.charAt(0))){
            throw new ErreurEntreException("Veuillez entrer un nombre !");
        }
        int ligne = Integer.parseInt(res);
        if(ligne > grille.getNbLignes()){
            throw new ErreurEntreException("Veillez à entrer un nombre valide !");
        }
        else{
            return ligne;
        }
    }

    public static int jouerColonne(String action, Grille grille) throws ErreurEntreException{
        Scanner sc = new Scanner(System.in);
        if(action.equals("r")){
            System.out.println("A quelle colonne se trouve la case que souhaitez vous révéler ?");
        }
        else if(action.equals("m")){
            System.out.println("A quelle colonne se trouve la case que souhaitez vous marquer ?");
        }
        String res = sc.nextLine();
        if(!Character.isDigit(res.charAt(0))){
            throw new ErreurEntreException("Veuillez entrer un nombre !");
        }
        int colonne = Integer.parseInt(res);
        if(colonne > grille.getNbLignes()){
            throw new ErreurEntreException("Veillez à entrer un nombre valide !");
        }
        else{
            return colonne;
        }
    }

    public static int nombreDeLignes(){
            Scanner sc = new Scanner(System.in);
            System.out.println("Combien de lignes voulez-vous pour ce plateau ?");
            String res = sc.nextLine();
            if(!Character.isDigit(res.charAt(0))){
                System.out.println("Veuillez entrez un nombre !");
            }
            return Integer.parseInt(res);
    }

    public static int nombreDeColonnes(){
            Scanner sc = new Scanner(System.in);
            System.out.println("Combien de colonnes voulez-vous pour ce plateau ?");
            String res = sc.nextLine();
            if(!Character.isDigit(res.charAt(0))){
                System.out.println("Veuillez entrez un nombre !");
            }
            return Integer.parseInt(res);
    }

    public static int nombreDeBombes(){
            Scanner sc = new Scanner(System.in);
            System.out.println("Combien de bombes voulez-vous pour ce plateau ?");
            String res = sc.nextLine();
            if(!Character.isDigit(res.charAt(0))){
                System.out.println("Veuillez entrez un nombre !");
            }
            return Integer.parseInt(res);
    }

    public static void main(String[] args) throws ErreurEntreException{
        

        int nbLignes = nombreDeLignes();
        int nbColonnes = nombreDeColonnes();
        int nbBombes = nombreDeBombes();

        Grille jeu = new Grille(nbLignes, nbColonnes, nbBombes);
        System.out.println("\nLe jeu du DEMINEUR\n");
        jeu.affiche();
            
        while(!(jeu.estGagnee() && jeu.estPerdue())){
            jeu.affiche();
            String action = action();
            int ligne = jouerLigne(action, jeu);
            int colonne = jouerColonne(action, jeu);
            Case laCase = jeu.getCase(ligne, colonne);
            if(action.equals("r")){
                laCase.reveler();
            }
            else{
                laCase.marquer();
            }
        }
        if(jeu.estPerdue()){
            System.out.println("La partie est perdue !\n");
        }
        else if(jeu.estGagnee()){
            System.out.println("Bravo vous avez gagné la partie !\n");
        }
        System.out.println("Merci d'avoir joué !\n");
    }

}