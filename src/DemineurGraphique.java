import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.control.Label;
import javafx.scene.control.Button; 
import javafx.geometry.Pos;

import javafx.scene.Node;
public class DemineurGraphique extends Application {

    private Grille lePlateau;
    private Pane infos;
    private GridPane grille;
    
    @Override
    public void init(){
        int nbParametres = this.getParameters().getRaw().size();
        try{
            int lignes = Integer.valueOf(this.getParameters().getRaw().get(0));
            int colonnes = Integer.valueOf(this.getParameters().getRaw().get(1));
            int nbBombes = Integer.valueOf(this.getParameters().getRaw().get(2)); 
            this.lePlateau = new Grille(lignes, colonnes, nbBombes);
        }
        catch(Exception e){
            this.lePlateau = new Grille(5, 5, 4);
        }
    }

    @Override
    public void start(Stage stage) {
        VBox vbox = new VBox(20);
        vbox.setAlignment(Pos.TOP_CENTER);
        this.grille = new GridPane();
        this.grille.setHgap(1);
        this.grille.setVgap(1);     
        for (int i = 0; i<this.lePlateau.getNbLignes(); i++){
            for (int j=0; j<this.lePlateau.getNbColonnes(); j++){
                Case laCase = this.lePlateau.getCase(i, j);
                Bouton b = new Bouton(laCase);

                b.setOnMouseClicked(new ControleurBouton(b, laCase, this, this.lePlateau));
                grille.add(b, i, j);
            }
        }
        this.infos = new VBox(); 
        vbox.getChildren().addAll(grille, infos);
        this.maj_des_infos();
        
        Scene scene = new Scene(vbox);
        stage.setTitle("Demineur");
        stage.setScene(scene);
        stage.show();
    }

    public void reload(){
        this.lePlateau.init();
        grille.getChildren().clear();
        for(int ligne = 0; ligne < this.lePlateau.getNbLignes(); ++ ligne){
            for(int colonne = 0; colonne < this.lePlateau.getNbColonnes(); ++ colonne){
                Case laCase = this.lePlateau.getCase(ligne, colonne);
                Bouton bLaCase = new Bouton(laCase);
                bLaCase.setOnMouseClicked(new ControleurBouton(bLaCase, laCase, this, this.lePlateau));
                grille.add(bLaCase, ligne, colonne);
            }
        }
    }

    public void maj_de_la_grille(){
        for (Node b : this.grille.getChildren()){
            Bouton bb = (Bouton) b;
            bb.maj();
        }
    }

    public void desactiver(){
        for (Node b : this.grille.getChildren()){
            b.setDisable(true);
        }
    }
    
    public void maj_des_infos(){
        this.infos.getChildren().clear();
        Label label1 = new Label("Nombres de bombes : " + this.lePlateau.getNbBombes());
        Label label2 = new Label("Nombres de cases marquées : " + this.lePlateau.getNombreDeCasesMarquees());
        Label label3 = new Label("Nombres de cases découvertes : " + this.lePlateau.getNombreDeCasesRevelees());
        this.infos.getChildren().addAll(label1, label2, label3);
    }
    
    public static void main(String args[]){
        Application.launch(args);
    }
}
