import java.util.*;

public class Grille{

    private int nbLignes;
    private int nbColonnes;
    private int nbBombes;
    private List<List<Case>> grille;

    public Grille(int nbLignes, int nbColonnes, int nbBombes) {
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.nbBombes = nbBombes;
        this.grille = new ArrayList<>();
        this.init();
    }

    //Getters
    public int getNbLignes(){return this.nbLignes;}
    public int getNbColonnes(){return this.nbColonnes;}
    public int getNbBombes(){return this.nbBombes;}
    public List<List<Case>> getGrille(){return this.grille;}
    public Case getCase(int ligne, int colonne){return this.grille.get(ligne).get(colonne);}

    public void init(){
        this.grille = new ArrayList<>();
        this.ajouterCases();
        this.ajouterCasesVoisines();
        this.ajouterBombes();
    }

    public void ajouterCases(){
        for(int ligne = 0; ligne < this.nbLignes; ++ ligne){
            List<Case> listeLigne = new ArrayList<>();
            for(int colonne = 0; colonne < this.nbColonnes; ++ colonne){
                listeLigne.add(new Case(ligne, colonne));
            }
            this.grille.add(listeLigne);
        } 
    }

    public void ajouterCasesVoisines(){
        for(int ligne = 0; ligne < this.nbLignes; ++ ligne){
            for(int colonne = 0; colonne < this.nbColonnes; ++ colonne){
                Case laCase = this.grille.get(ligne).get(colonne);
                // Case du haut
                if(colonne > 0){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne).get(colonne - 1));
                }
                // Case du bas
                if(colonne < this.nbColonnes - 1){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne).get(colonne + 1));
                }
                // Case de gauche
                if(ligne > 0){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne - 1).get(colonne));
                }
                // Case de droite
                if(ligne < this.nbLignes - 1){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne + 1).get(colonne));
                }
                // Case en haut à gauche
                if(ligne > 0 && colonne > 0){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne - 1).get(colonne - 1));
                }
                // Case en haut à droite
                if(ligne < this.nbLignes - 1 && colonne > 0){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne + 1).get(colonne - 1));
                }
                // Case en bas à gauche
                if(ligne > 0 && colonne < this.nbColonnes - 1){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne - 1).get(colonne + 1));
                }
                // Case en bas à droite
                if(ligne < this.nbLignes - 1 && colonne < this.nbColonnes - 1){
                    laCase.ajouteCaseVoisine(this.grille.get(ligne + 1).get(colonne + 1));
                }
            }
        }
    }

    public void ajouterBombes(){
        int max = 0;
        while(max <= this.nbBombes){
            int ligne = (int) (Math.random()*this.nbLignes);
            int colonne = (int) (Math.random()*this.nbColonnes);
            Case laCase = this.getCase(ligne, colonne);
            laCase.ajouteBombe();
            ++ max;
        }
    }

    public int getNombreDeCasesRevelees(){
        int nbCasesRevelees = 0;
        for(int ligne = 0; ligne < this.nbLignes; ++ ligne){
            for(int colonne = 0; colonne < this.nbColonnes; ++ colonne){
                if((this.getCase(ligne, colonne)).estRevelee()){
                    ++ nbCasesRevelees;
                }
            }
        }
        return nbCasesRevelees;
    }

    public int getNombreDeCasesMarquees(){
        int nbCasesMarquees = 0;
        for(int ligne = 0; ligne < this.nbLignes; ++ ligne){
            for(int colonne = 0; colonne < this.nbColonnes; ++ colonne){
                if((this.getCase(ligne, colonne)).estMarquee()){
                    ++ nbCasesMarquees;
                }
            }
        }
        return nbCasesMarquees;
    }

    public boolean estPerdue(){
        for(List<Case> listeCase : this.grille){
            for(Case laCase : listeCase){
                if(laCase.getAffichage() == "@"){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean estGagnee(){
        if(this.getNombreDeCasesMarquees() == this.nbBombes){
            int cpt = 0;
            for(List<Case> listeCase : this.grille){
                for(Case laCase : listeCase){
                    if(laCase.estBombe() && laCase.estMarquee()){
                        ++ cpt;
                    }
                }
            }
            if(this.nbBombes == cpt){
                return true;
            }
        }
        return false;
    }

    public void affiche() {
        String affiche = "    ";
        for (int colonne = 0; colonne < nbColonnes; colonne++) {
            affiche += colonne + "   ";
        }
        affiche += "\n  ┌";
        for (int colonne = 0; colonne < nbColonnes - 1; colonne++) {
            affiche += "───┬";
        }
        affiche += "───┐";
        for (int ligne = 0; ligne < nbLignes; ligne++) {
            if (ligne > 0) {
                affiche += "\n  ├───";
                for (int colonne = 0; colonne < nbColonnes - 1; colonne++) {
                    affiche += "┼───";
                }
                affiche += "┤\n" + ligne + " │ ";
            } else {
                affiche += "\n" + ligne + " │ ";
            }
            for (int colonne = 0; colonne < nbColonnes; colonne++) {
                affiche += this.getCase(ligne, colonne).getAffichage() + " │ ";
            }
        }
        affiche += "\n  └";
        for (int colonne = 0; colonne < nbColonnes - 1; colonne++) {
            affiche += "───┴";
        }
        affiche += "───┘";
        System.out.println(affiche);
    }

}